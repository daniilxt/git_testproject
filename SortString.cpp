#include <iostream>
#include <algorithm>
#include <vector>


void PrintVector (const std:: vector<std:: string>& v){
  for (const auto& i: v){
    std:: cout << i << " ";
  }
}
/*std:: vector <std:: string> LowerCase (std:: vector<std:: string>& v){
  for (int i = 0; i < v.size(); i++){
  std::string data = v[i];
  std::transform(data.begin(), data.end(), data.begin(), ::tolower);
  v[i] = data;
 // std::cout <<data;
}
    return v;
} */
std::string LowerCase (std::string& s) {
  std::string temp = "";
  for (int i = 0; i < s.size(); ++i) {
    temp += tolower(s[i]);
  }
  return temp;
}

bool Flag (std::string i, std::string j) {
  return LowerCase(i) < LowerCase(j);
}

int main() {
  std:: vector <std::string> v;
  std:: string s;
  int x;
  std:: cin >> x;

  for(int i = 0; i < x; i++){
    std:: cin >> s;
    v.push_back(s);
  }
  //LowerCase(v);
  sort(begin(v), end(v),Flag);
  PrintVector(v);
  return 0;
}